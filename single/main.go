package main

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"regexp"
	"time"

	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

/**MODELS*/

type Assignee struct {
	ID         string `json:"_id,omitempty" bson:"_id,omitempty"`
	Name       string `json:"name"`
	Department string `json:"deparmant"`
}

type Candidate struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	Firstname       string    `json:"first_name" bson:"first_name"`
	Lastname        string    `json:"last_name" bson:"last_name"`
	Email           string    `json:"email" bson:"email"`
	Department      string    `json:"department" bson:"departmant"`
	University      string    `json:"university" bson:"university"`
	Experience      bool      `json:"experience" bson:"experience"`
	Status          string    `json:"status" bson:"status"`
	MeetingCount    int       `json:"meeting_count" bson:"meeting_count"`
	NextMeeting     time.Time `json:"next_meeting" bson:"next_meeting"`
	Assignee        string    `json:"assignee" bson:"assignee"`
	ApplicationDate time.Time `json:"application_date,omitempty" bson:"application_date,omitempty"`
}

type NewMeeting struct {
	CandidateID     string `json:"candidateId"`
	NextMeetingTime string `json:"nextMeetingTime"`
}

/**UTILS*/

func ConnectDb(collectionName string) *mongo.Collection {
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27018")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}
	collection := client.Database("Otsimo").Collection(collectionName)
	return collection
}

type ErrorResponse struct {
	StatusCode   int    `json:"status"`
	ErrorMessage string `json:"message"`
}

func CreateError(err error, w http.ResponseWriter) {
	var response = ErrorResponse{
		ErrorMessage: err.Error(),
		StatusCode:   http.StatusInternalServerError,
	}

	message, _ := json.Marshal(response)
	log.Println(response)

	w.WriteHeader(response.StatusCode)
	w.Write(message)
}

// getCandidates godoc
// @Summary Get all candidates
// @Description Get all candidates
// @Tags candidates
// @Accept  json
// @Produce  json
// @Success 200 {array} Candidate
// @Router /candidates [get]
func getCandidates(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Candidates")
	var candidates []Candidate
	cur, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		CreateError(err, w)
		return
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		var candidate Candidate
		err := cur.Decode(&candidate)
		if err != nil {
			log.Println(err)
		}

		candidates = append(candidates, candidate)
	}

	if err := cur.Err(); err != nil {
		CreateError(err, w)
		return
	}

	json.NewEncoder(w).Encode(candidates)
}

func retrieveCandidate(id string, w http.ResponseWriter) (Candidate, bool) {
	var collection = ConnectDb("Candidates")
	fmt.Println(id)
	var candidate Candidate
	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&candidate)

	if err != nil {
		CreateError(err, w)
		return candidate, true
	}
	return candidate, false
}

// getCandidate godoc
// @Summary Get candidate by id
// @Description Get candidate by id
// @Tags candidates
// @Accept  json
// @Produce  json
// @Success 200 {object} Candidate
// @Param id path string true "candidate id"
// @Router /candidates/{id} [get]
func getCandidate(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")
	var params = mux.Vars(r)
	id, _ := params["id"]
	candidate, err := retrieveCandidate(id, w)
	if err != true {
		json.NewEncoder(w).Encode(candidate)
	}
}

// deleteCandidate godoc
// @Summary Delete candidate by id
// @Description Delete candidate by id
// @Tags candidates
// @Accept  json
// @Produce  json
// @Success 200 {object} Candidate
// @Param id path string true "candidate id"
// @Router /candidates/{id} [delete]
func deleteCandidate(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Candidates")

	var params = mux.Vars(r)

	id, _ := params["id"]

	filter := bson.M{"_id": id}
	result, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		CreateError(err, w)
		return
	}

	json.NewEncoder(w).Encode(result)
}

/*
Checks new candidates assignee
*/
func checkAssignee(department string, assigneeId string, w http.ResponseWriter) bool {
	var collection = ConnectDb("Assignees")
	var assignee Assignee
	filter := bson.M{"_id": assigneeId}
	err := collection.FindOne(context.TODO(), filter).Decode(&assignee)

	if err != nil {
		CreateError(errors.New("Assignee id is wrong."), w)
		return false
	}
	if assignee.Department != department {
		CreateError(errors.New("Assignee departmant is not matched with candidate's departmant!"), w)
		return false
	}
	return true
}

func checkEmail(email string, w http.ResponseWriter) bool {
	var emailRegex = regexp.MustCompile(`([a-zA-Z0-9]+)@([a-zA-Z0-9\.]+)\.([a-zA-Z0-9]+)`)
	if emailRegex.MatchString(email) == false {
		CreateError(errors.New("Email format is wrong"), w)
		return false
	}
	return true
}

func generateId() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	uuid := fmt.Sprintf("%x-%x-%x-%x-%x",
		b[0:4], b[4:6], b[6:8], b[8:10], b[10:])

	return uuid
}

// createCandidate godoc
// @Summary Create a new candidate
// @Description Create a new candidate with the input paylod
// @Tags candidates
// @Accept  json
// @Produce  json
// @Param candidate body NewCandidate true "Create candidate"
// @Success 200 string inserted id
// @Router /candidates [post]
func createCandidate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Candidates")

	var candidate Candidate
	_ = json.NewDecoder(r.Body).Decode(&candidate)

	candidate.ID = generateId()
	fmt.Println(candidate)

	checkAssigneeResult := checkAssignee(candidate.Department, candidate.Assignee, w)
	if !checkAssigneeResult {
		return
	}
	checkEmailResult := checkEmail(candidate.Email, w)
	if !checkEmailResult {
		return
	}
	candidate.Status = "Pending"
	candidate.MeetingCount = 0
	result, err := collection.InsertOne(context.TODO(), candidate)
	if err != nil {
		CreateError(err, w)
		return
	}
	json.NewEncoder(w).Encode(result)
}

// arrangeMeeting godoc
// @Summary Arrange candidate meeting
// @Description Arrange candidate meeting by id
// @Tags candidate-meeting
// @Accept  json
// @Produce  json
// @Param newmeeting body NewMeeting true "Arrange Meeting"
// @Success 200 {object} Candidate
// @Router /arrange-meeting [post]
func arrangeMeeting(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Candidates")

	var candidate Candidate
	var newMeeting NewMeeting
	_ = json.NewDecoder(r.Body).Decode(&newMeeting)

	t, errParse := time.Parse(time.RFC3339, newMeeting.NextMeetingTime)
	fmt.Println(errParse)

	curCandidate, errRes := retrieveCandidate(newMeeting.CandidateID, w)
	if errRes != false {
		return
	}

	update := bson.D{
		{"$set", bson.D{
			{"next_meeting", t},
		}},
	}
	if curCandidate.MeetingCount == 3 { // in case of next week is fourth one
		update = bson.D{
			{"$set", bson.D{
				{"next_meeting", t},
				{"assignee", "5c191acea7948900011168d4"},
			}},
		}
	}

	upsert := false
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}

	filter := bson.M{"_id": newMeeting.CandidateID}
	err := collection.FindOneAndUpdate(context.TODO(), filter, update, &opt).Decode(&candidate)

	if err != nil {
		CreateError(err, w)
		return
	}

	json.NewEncoder(w).Encode(candidate)
}

// completeMeeting godoc
// @Summary Complete meeting
// @Description Complete meeting
// @Tags candidate-meeting
// @Accept  json
// @Produce  json
// @Param id path string true "candidate id"
// @Success 200 {object} Candidate
// @Router /complete-meeting/{id} [post]
func completeMeeting(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Candidates")

	var candidate Candidate
	var params = mux.Vars(r)

	id, _ := params["id"]

	curCandidate, errGet := retrieveCandidate(id, w)
	if errGet != false {
		return
	}
	update := bson.D{
		{"$inc", bson.D{
			{"meeting_count", 1},
		}},
	}
	if curCandidate.MeetingCount+1 < 4 { // in case of num of meeting is less than 4
		update = bson.D{
			{"$set", bson.D{
				{"status", "In Progress"},
			}},
			{"$inc", bson.D{
				{"meeting_count", 1},
			}},
		}
	}

	filter := bson.M{"_id": id}
	upsert := false
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}
	err := collection.FindOneAndUpdate(context.TODO(), filter, update, &opt).Decode(&candidate)

	if err != nil {
		CreateError(err, w)
		return
	}
	json.NewEncoder(w).Encode(candidate)
}

// acceptCandidate godoc
// @Summary Accept candidate
// @Description Accept candidate
// @Tags candidate-status
// @Accept  json
// @Produce  json
// @Param id path string true "candidate id"
// @Success 200 {object} Candidate
// @Router /accept-candidate/{id} [post]
func acceptCandidate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Candidates")

	var candidate Candidate
	var params = mux.Vars(r)

	id, _ := params["id"]

	curCandidate, errGet := retrieveCandidate(id, w)
	if errGet != false {
		return
	}
	if curCandidate.MeetingCount < 4 {
		CreateError(errors.New("Number of meeting of candidate is insufficient"), w)
		return
	}
	update := bson.D{
		{"$set", bson.D{
			{Key: "status", Value: "Accepted"},
		}},
	}
	upsert := false
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}
	filter := bson.M{"_id": id}
	err := collection.FindOneAndUpdate(context.TODO(), filter, update, &opt).Decode(&candidate)

	if err != nil {
		CreateError(err, w)
		return
	}

	json.NewEncoder(w).Encode(candidate)
}

// denyCandidate godoc
// @Summary Denny candidate
// @Description Denny candidate
// @Tags candidate-status
// @Accept  json
// @Produce  json
// @Param id path string true "candidate id"
// @Success 200 {object} Candidate
// @Router /deny-candidate/{id} [post]
func denyCandidate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Candidates")

	var candidate Candidate

	// prepare update model.
	update := bson.D{
		{"$set", bson.D{
			{Key: "status", Value: "Denied"},
		}},
	}
	var params = mux.Vars(r)

	id, _ := params["id"]

	filter := bson.M{"_id": id}

	upsert := false
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}
	err := collection.FindOneAndUpdate(context.TODO(), filter, update, &opt).Decode(&candidate)

	if err != nil {
		CreateError(err, w)
		return
	}
	json.NewEncoder(w).Encode(candidate)
}

func retrieveAssignee(id string, w http.ResponseWriter) Candidate {
	var collection = ConnectDb("Candidates")

	var candidate Candidate
	filter := bson.M{"_id": id}
	err := collection.FindOne(context.TODO(), filter).Decode(&candidate)

	if err != nil {
		CreateError(err, w)
	}
	return candidate
}

// findAssigneeIDByName godoc
// @Summary findAssigneeIDByName
// @Description findAssigneeIDByName
// @Tags assignees
// @Accept  json
// @Produce  json
// @Param name path string true "assignee name"
// @Success 200 {string} {_id}
// @Router /find-assignee-by-name/{name} [get]
func findAssigneeIDByName(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Assignees")

	var params = mux.Vars(r)

	name, _ := params["name"]

	result := struct {
		ID string `json:"_id,omitempty" bson:"_id,omitempty"`
	}{}

	filter := bson.M{"name": name}
	err := collection.FindOne(context.TODO(), filter).Decode(&result)

	if err != nil {
		CreateError(err, w)
		return
	}

	json.NewEncoder(w).Encode(result)
}

// findAssigneesCandidates godoc
// @Summary findAssigneesCandidates
// @Description findAssigneesCandidates
// @Tags assignees
// @Accept  json
// @Produce  json
// @Param assigneeId path string true "assignee id"
// @Success 200 {array} Candidate
// @Router /find-assignee-candidates/{assigneeId} [get]
func findAssigneesCandidates(w http.ResponseWriter, r *http.Request) {
	// set header.
	w.Header().Set("Content-Type", "application/json")
	var collection = ConnectDb("Candidates")

	var params = mux.Vars(r)

	id, _ := params["assigneeId"]

	filter := bson.M{"assignee": id}
	cur, err := collection.Find(context.TODO(), filter)
	var candidates []Candidate

	if err != nil {
		CreateError(err, w)
		return
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {

		var candidate Candidate
		err := cur.Decode(&candidate)
		if err != nil {
			log.Println(err)
		}

		candidates = append(candidates, candidate)
	}

	if err := cur.Err(); err != nil {
		CreateError(err, w)
		return
	}
	json.NewEncoder(w).Encode(candidates)
}
func handleRequests() {
	router := mux.NewRouter()
	router.HandleFunc("/candidates", getCandidates).Methods("GET")
	router.HandleFunc("/candidates/{id}", getCandidate).Methods("GET")
	router.HandleFunc("/candidates", createCandidate).Methods("POST")
	router.HandleFunc("/candidates/{id}", deleteCandidate).Methods("DELETE")
	router.HandleFunc("/arrange-meeting", arrangeMeeting).Methods("POST")
	router.HandleFunc("/complete-meeting/{id}", completeMeeting).Methods("POST")
	router.HandleFunc("/accept-candidate/{id}", acceptCandidate).Methods("POST")
	router.HandleFunc("/deny-candidate/{id}", denyCandidate).Methods("POST")
	router.HandleFunc("/find-assignee-by-name/{name}", findAssigneeIDByName).Methods("GET")
	router.HandleFunc("/find-assignee-candidates/{assigneeId}", findAssigneesCandidates).Methods("GET")

	// Swagger
	router.PathPrefix("/doc").Handler(httpSwagger.WrapHandler)
	log.Fatal(http.ListenAndServe(":8080", router))
}

// @title Otsimo API
// @version 1.0
// @description This is a sample restful APIs for otsimo summer talent camp project.
// @contact.name Gokturk Topar
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:8080
// @BasePath /

func main() {
	fmt.Println("Server Started...")
	handleRequests()
}
