# otsimo-restful-APIs

This project was written golang 1.15.6. Single folder contains single file solution, structure one has seperated packages, also contains swagger documentation package. Also postman collection can be used to monitoring APIs.

# Getting started

### Single folder
Launch development server, and open `localhost:8080` in your browser:
 ```sh
 go run main.go
 ```

### Structured folder
 Launch development server, and open `localhost:8080/doc/index.html` in your browser, hitting that path provide api documentation.
 ```sh
 cd app
 go run main.go
 ```




