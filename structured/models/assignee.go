package models

type Assignee struct {
	ID         string `json:"_id,omitempty" bson:"_id,omitempty"`
	Name       string `json:"name"`
	Department string `json:"deparmant"`
}
