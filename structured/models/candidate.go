package models

import (
	"time"
)

type Candidate struct {
	ID              string    `json:"_id,omitempty" bson:"_id,omitempty"`
	Firstname       string    `json:"first_name" bson:"first_name"`
	Lastname        string    `json:"last_name" bson:"last_name"`
	Email           string    `json:"email" bson:"email"`
	Department      string    `json:"department" bson:"departmant"`
	University      string    `json:"university" bson:"university"`
	Experience      bool      `json:"experience" bson:"experience"`
	Status          string    `json:"status" bson:"status"`
	MeetingCount    int       `json:"meeting_count" bson:"meeting_count"`
	NextMeeting     time.Time `json:"next_meeting" bson:"next_meeting"`
	Assignee        string    `json:"assignee" bson:"assignee"`
	ApplicationDate time.Time `json:"application_date,omitempty" bson:"application_date,omitempty"`
}

type NewMeeting struct {
	CandidateID     string `json:"candidateId"`
	NextMeetingTime string `json:"nextMeetingTime"`
}
